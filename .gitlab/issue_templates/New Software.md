# Introduction

Use this template to request the addition of new software to versionScan's inventory.

# Necessary information

Please provide the following information (you can copy the list if you want to request
the addition of several softwares):

* __Software name__:  _Specify here the name of the software of your concern_.
* __URL__: _Specify here the URL where to find the latest version number of your software_.
* __CPE-like identifier__: _Specify here a *CPE-like identifier* of your software.
  Please look at https://cve.circl.lu/browse to devise if an official CPE dictionary 
  entry already exists. 
  The format of the CPE-like identifier is: "<vendor>:<softwarename>".
  Please refer to [NISTIR-7695](https://csrc.nist.gov/projects/security-content-automation-protocol/specifications/cpe/naming)
  §5.3.2 and §5.5.3 for details on how "<vendor>" and "<software>" should be formatted
  (mainly only printable ASCII characters and no blank spaces).
