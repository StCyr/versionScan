#!/usr/bin/env perl

use strict;
use warnings;
use v5.10;
no warnings 'experimental::smartmatch';

use versionScan;
use versionScan::Core::Engine;

use File::Path 'rmtree';
use JSON;
use LWP::Simple;
use Win32::Exe;   # for "inspect" method
use List::Util 'shuffle';
use XML::LibXML;  # for "inspect" method
use Data::Dumper; # For debugging purpose

# Get a software model handle
my $app = versionScan->new();
my $engine = versionScan::Core::Engine->new();

####################################################################
#
# Process watch files, distribution files and update DB functions
#
# ##################################################################

sub _getPackageList {
  my ($url, $regex) = @_; 
  my $packageList = {};

  # Get platform's package list page
  my $content = get($url); 
 
  # Parse page to get a list of all platform's packages, with version(s) and applying architecture(s)
  while ($content =~ /$regex/g) {
    my $name = $1;
    my @versions = split('],',$2); 

    if ($#versions == 0) {

      # There's only 1 version reported, so it applies to all architectures
      $packageList->{$name}->{'any'} = $versions[0];

    } else {

      # There are several versions reported, so we need to assign each version to its architecture.
      foreach (@versions) {
        my ($version,$arch) = split('\[',$_);
        $version =~ s/^\s+|\s+$//g;

        foreach my $archi ( split(',', $arch) ) {
          $archi =~ s/^\s+|]$//g;
          $packageList->{$name}->{$archi} = $version;
        }

      }

    }

  }
  
  return $packageList;
}

############################################################
#
# processDistributionFiles:
#    Make sure all packages of a distribution are in the DB
#
############################################################

sub processDistributionFiles {

    my @watchFiles = @_;
    unless ( @watchFiles ) { @watchFiles = `cd watch/distrib;ls -1` };

DFILE:
    # Process each watch file
    foreach (@watchFiles) {

        my $soft = $_;
        chomp($soft);

        # Open watch file
        print "Processing distribution file $soft\n";
        my $soft_file = "watch/distrib/$soft";
        unless (open WATCH,"<$soft_file") {
            print "Error while trying to open file $soft_file\n";
            next DFILE;
        }

        # Read specific versionScan info
        my ($name,$nameAliases,$publisher,$pubAliases,$pListUrl,$pVirtualListUrl,$pRegex,$pVirtualRegex,$pUrl) = ('','','','','','','','','');
        while (<WATCH>) {
            given ($_) {
                when (/^\s*\#\s*Name\s*:\s*(.*)$/) { $name = $1 };
                when (/^\s*\#\s*Name Aliases\s*:\s*(.*)$/) { $nameAliases = $1 };
                when (/^\s*\#\s*Publisher\s*:\s*(.*)$/) { $publisher = $1 };
                when (/^\s*\#\s*Publisher Aliases\s*:\s*(.*)$/) { $pubAliases = $1 };
                when (/^\s*\#\s*PackageListUrl\s*:\s*(.*)$/) { $pListUrl = $1 };
                when (/^\s*\#\s*PackageVirtualListUrl\s*:\s*(.*)$/) { $pVirtualListUrl = $1 };
                when (/^\s*\#\s*PackageRegex\s*:\s*(.*)$/) { $pRegex = $1 };
                when (/^\s*\#\s*PackageVirtualRegex\s*:\s*(.*)$/) { $pVirtualRegex = $1 };
                when (/^\s*\#\s*PackageUrl\s*:\s*(.*)$/) { $pUrl = $1 };
            }
        }

        if ($name eq '' or $publisher eq '' or $pListUrl eq '' or $pRegex eq '' or $pUrl eq '') {
            print "Cannot find all required versionScan info in distribution file $soft_file, skipping.\n";
            next DFILE;
        }

	# Get Platform ID
        my $platformId = $app->model('platform')->getIdFromName($name);
	if ($platformId eq '') {
          $platformId = $app->model('platform')->createID($name);
	}

        # Register all Platform aliases
	if ($nameAliases ne '') {
            foreach my $distrib ( split(',', $nameAliases) ) {
                 $app->model('platform')->addAlias($platformId,$distrib);
            }
        }

        # Get Publisher ID
        my $publisherId = $app->model('publisher')->getIdFromName( $publisher );
	if ($pubAliases ne '') {
            foreach my $pub ( split(',', $pubAliases) ) {
                $pub =~ s/^\s+|\s+$//g;
                $app->model('publisher')->addAlias($publisherId,$publisher);
            }
        }

        # Get platform's packages list
        my $packageList = _getPackageList( $pListUrl, $pRegex );

	# Get platform's virtual packages list, if provided
        my $vPackageList;
	if ($pVirtualListUrl ne '') {
		if ($pVirtualRegex ne '') {
        		$vPackageList = _getPackageList( $pVirtualListUrl, $pVirtualRegex );
		} else {
            		print "Warning: pVirtualListUrl parameter provided but no pVirtualRegex => will not filter virtual packages.\n";
		}
	}

	# Parse package list
        while ( my $package = each %$packageList  ) {

                # Get versionScan's sofware ID
      		my $vscanSwId = $app->model('software')->getVscanID($app, $package, $publisher);

		if ( not exists($vPackageList->{$package}->{'any'}) ) { 

			while ( my $arch = each %{$packageList->{$package}} ) {

				print "found $package for architecture $arch with version $packageList->{$package}->{$arch} (publisher is $publisher)\n";

		                # Create a versionScan's sofware ID if needed
		     	        if ( $vscanSwId =~ /^Error/ ) {
		                	$vscanSwId = $app->model('software')->createVscanID($app, $package, $publisher);
			        }

		                # Compute "check_method" field
		                # TODO: This is very Debian specific
			        (my $package_quoted = $package) =~ s/\+/\\\\+/g;
			        my $cm  = '[{"step":"uscan","parameters":{"watchline":"opts=\\"searchmode=plain\\" ' . $pUrl . '/' . $package . ' ' . $package_quoted . '\\\\s\\\\(([^\\\\)]+) ignore"}}]';

		                # Update release info
		    	        $app->model('release')->updateInfo($vscanSwId, $platformId, $arch, $packageList->{$package}->{$arch}, $cm, "$pUrl/$package");

			}

		} else {
			print "found virtual package $package => deleting all already registered releases for this platform, if any.\n";
	     	        if ( $vscanSwId !~ /^Error/ ) {
	    	        	$app->model('release')->delete($vscanSwId, $platformId);
				print "  All releases of package $package for platform $name have been deleted.\n";
			}
		}

        }

    }

}

##################################################
#
# processWatchfiles: 
#    make sure releases in the DB are up-to-date
#    with their watchfiles counterpart. 
#    It doesn't verify latest release versions.
#
##################################################
sub processWatchfiles {

    # Get the name of watch files to process either from the command-line argument
    # or via all files in the './watch' directory.
    my @watchFiles = @_;
    unless ( @watchFiles ) { @watchFiles = `cd watch;ls -1 -I distrib` };

WATCHFILE:
    # Process each watch file
    foreach (@watchFiles) {

        my $soft = $_;
        chomp($soft);

        # Open watch file
        print "Processing watch file $soft\n";
        my $soft_file = "watch/$soft";
        unless (open WATCH,"<$soft_file") {
            print "Error while trying to open file $soft_file\n";
            next WATCHFILE;
        }

        # Read specific versionScan info
        my ($names,$publishers,$platform) = '';
        my %check_methods;
        while (<WATCH>) {
            given ($_) {
                when (/^\s*\#\s*Names\s*:\s*(.*)$/) { $names = $1 };
                when (/^\s*\#\s*Publishers\s*:\s*(.*)$/) { $publishers = $1 };
                when (/^\s*\#\s*Platform\s*:\s*(.*)$/) { $platform = $1 };
                when (/^\s*\#\s*Check Methods\s*:\s*(.*)$/) { $check_methods{$platform} = $1 };
            }
        }

        if ($names eq '' or $publishers eq '' or $platform eq '' or !%check_methods) {
            print "Cannot find versionScan info in watch file $soft_file, skipping.\n";
            next WATCHFILE;
        }

        # Initialize uscan config
        my $uscanConfig = Devscripts::Uscan::Config->new->parse;
	
        # For each software and publisher name tulpes given, get the latest version for every platform given
        my ($vscanSwId, $softwareId, $publisherId) = ('','','');
        foreach my $package ( split(',', $names) ) {

            $package =~ s/^\s+|\s+$//g;

	    # Getting a software ID, creating a new one or aliases if needed
            if ($softwareId eq '') {
                $softwareId = $app->model('software')->getIdFromName($package);
		    if (not defined $softwareId) {
                        $softwareId = $app->model('software')->createID($package);
		    }
            } else {
                $app->model('software')->addAlias($softwareId,$package);
            }

            foreach my $publisher ( split(',', $publishers) ) {

                $publisher =~ s/^\s+|\s+$//g;

		# Getting a publisher ID, creating a new one or aliases if needed
                if ($publisherId eq '') {
                    $publisherId = $app->model('publisher')->getIdFromName($publisher);
		    if (not defined $publisherId) {
			print "  Creating an ID for publisher $publisher\n";
                        $publisherId = $app->model('publisher')->createID($publisher);
		    }
                } else {
		    my $pubNames = $app->model('publisher')->getNamesFromID($publisherId);
		    if ( not $pubNames->{$publisher} ) {
	  	        print "  Creating an alias to ID $publisherId for publisher $publisher\n";
                        $app->model('publisher')->addAlias($publisherId,$publisher);
		    }
                }

                # Get versionScan's sofware ID or create one if needed
                $vscanSwId = $app->model('software')->getVscanID($app, $package,$publisher);
	        if ( $vscanSwId =~ /^Error/ ) {
                    $vscanSwId = $app->model('software')->createVscanID($app, $package, $publisher);
	        }

PLATFORM:
                foreach my $platform (keys %check_methods) {
                    my $result = { 'status' => 0 };
	 
                    # Get Platform ID
                    my ($distrib,$architecture) = split(/\//,$platform);
        	    my $platformId = $app->model('platform')->getIdFromName($distrib);
		    if ($platformId eq '') {
          		$platformId = $app->model('platform')->createID($distrib);
		    }

		    # Check if the release already exists in the DB, or if its info must be updated
	            if ( not $app->model('release')->isUptodate($vscanSwId, $platformId,$architecture, $check_methods{$platform}) ) { 
	                print "  Release ($vscanSwId/$package/$publisher/$platform) exists and already up-to-date.\n";
	                next PLATFORM;
	            }

	            # Create a temporary directory
	            mkdir 'tmp';
	            chdir 'tmp';

		    # Execute all check_methods' steps to retrieve the release's latest version
                    for my $step ( @{decode_json($check_methods{$platform})} ) {

                        print "  Executing step $step->{'step'} for $package from $publisher ($soft) on platform $distrib($architecture).\n";
                        given ( $step->{'step'} ) {
                            when ('download')          { $result = $engine->download($result, $step->{'parameters'}) };
                            when ('uscan')             { $result = $engine->uscan($result, $step->{'parameters'}, $uscanConfig, $soft) };
                            when ('extract')           { $result = $engine->extract($result, $step->{'parameters'}) };
                            when ('inspect')           { $result = $engine->inspect($result, $step->{'parameters'}) };
                        }

                        # Make sure step was OK
                        if ( $result->{status} ) {
                            print "Error while trying executing step $step->{'step'}\n";
			    chdir '..';
			    rmtree('tmp');
                            next PLATFORM;
                        }

                    }

		    # Replace dash and underscore separators by dots (only for versions containing only dashes and underscores)
		    if ( $result->{'last_version'} =~ m/^[-_\d]+$/ ) {
                        $result->{'last_version'} =~ tr/-_/./;
		    }

                    print "  Found version $result->{'last_version'}, updating release information.\n";

                    # Update release info
                    $app->model('release')->updateInfo($vscanSwId, $platformId,$architecture, $result->{'last_version'}, $check_methods{$platform}, $result->{'url'});

	            # Empty tmp folder
	            chdir '..';
	            rmtree('tmp');
                }

            }

        }

    }

}

sub deleteEntry {

    foreach my $vscan_sw ( @_ ) {
        $app->model('release')->delete($vscan_sw);
	print "$vscan_sw entries deleted\n";	
    }

}

sub updateDB {

    my @releases;

    # Get all vscan softwares when none have been provided
    if (not @_) {
       @releases = $app->model('release')->getAll();
    } else {
        foreach my $vscan_sw ( @_ ) {
           @releases = $app->model('release')->getDetails($vscan_sw);
	}
    }

RELEASE:

    # Update all releases
    foreach my $release (@releases) {

	my $vscan_sw_id   = %$release{'vscan_sw_id'};
        my $platform_id   = %$release{'platform_id'};
	my $architecture  = %$release{'architecture'};
	my $check_methods = %$release{'check_methods'};

        print "Processing release $vscan_sw_id\n";

        # Initialize uscan config
        my $uscanConfig = Devscripts::Uscan::Config->new->parse;

        # Create a temporary directory
        mkdir 'tmp';
        chdir 'tmp';

        # Follow all steps needed to get the software's latest version
        my $result = { 'status' => 0 };
        for my $step ( @{decode_json($check_methods)} ) {

            print "  Executing step $step->{'step'}\n";

            given ( $step->{'step'} ) {
                when ('download')          { $result = $engine->download($result, $step->{'parameters'}) };
                when ('uscan')             { $result = $engine->uscan($result, $step->{'parameters'}, $uscanConfig, $vscan_sw_id) };
                when ('extract')           { $result = $engine->extract($result, $step->{'parameters'}) };
                when ('inspect')           { $result = $engine->inspect($result, $step->{'parameters'}) };
            }

            # Make sure step was OK
            if ( $result->{status} ) {
                print "Error while trying executing step $step->{'step'} for $vscan_sw_id\n";
                my $sth = $app->model('software')->dbh->prepare('UPDATE releases SET last_check = NOW() WHERE vscan_sw_id = ? AND platform_id = ? AND architecture = ?');
                $sth->execute($vscan_sw_id, $platform_id, $architecture);
                chdir '..';
                rmtree('tmp');
                next RELEASE;
            }

        }

        # Replace dash and underscore separators by dots (only for versions containing only dashes and underscores)
        if ( $result->{'last_version'} =~ m/^[-_\d]+$/ ) {
           $result->{'last_version'} =~ tr/-_/./;
        }
 
        print "  Found version $result->{'last_version'}, updating database.\n";

        # Update release info
        $app->model('release')->updateInfo($vscan_sw_id, $platform_id, $architecture, $result->{'last_version'}, $check_methods, $result->{'url'});

        # Empty tmp folder
        chdir '..';
        rmtree('tmp');

    }

}

####################################################################
#
# Main program
#
# ##################################################################

if (@ARGV) {
    given($ARGV[0]) {
        when('distrib') { shift @ARGV; processDistributionFiles(@ARGV) }
        when('watch')   { shift @ARGV; processWatchfiles(@ARGV) }
	when('update')  { shift @ARGV; updateDB(@ARGV) }
	when('delete')  { shift @ARGV; deleteEntry(@ARGV) }
	default { print "Action not recognized. Only \"delete\", \"distrib\", \"update\", and \"watch\" are valid actions.\n"; }
    }
} else {
    # No argument given, perform all actions
    processWatchfiles();
    updateDB();
    processDistributionFiles();
}

=head1 NAME

update_db.pl - Updates the versionScan database

=head1 SYNOPSIS

From the top of the versionScan project, type:

  perl -Ilib script/update_db.pl

=head1 DESCRIPTION

Update latest version and URL of every entries in the database.

=head1 AUTHORS

Cyrille Bollu <cyrille@bollu.be>

=head1 COPYRIGHT

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
