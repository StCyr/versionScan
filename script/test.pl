#!/usr/bin/env perl

use strict;
use warnings;

my @testFiles = `ls -1 ./t/*.t`;
foreach (@testFiles) {

  print "$_";
  system('perl -Ilib ' .  $_);

}

=head1 AUTHORS

Cyrille Bollu <cyrille@bollu.be>

=head1 COPYRIGHT

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
