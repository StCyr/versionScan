#!/usr/bin/env perl

use strict;
use warnings;
use v5.10;
no warnings 'experimental::smartmatch';

use versionScan;
use versionScan::Core::Engine;

use JSON;
use File::Path 'rmtree';
use Data::Dumper; # For debugging purpose

# Get a software model handle
my $app = versionScan->new();
my $swModel = $app->model('software');
my $engine = versionScan::Core::Engine->new();

# Did we specified a software in argument
my @softwareList;
if (@ARGV) {
  @softwareList = @ARGV;
} else {
  @softwareList = `ls -1 watch`;
}

SOFT:
foreach my $soft (@softwareList) {

    # Open watch file
    chomp($soft);
    print "Processing file $soft\n";
    my $soft_file = "watch/$soft";
    unless (open WATCH,"<$soft_file") {
      print "Error while trying to open file $soft_file\n";
      next SOFT;
    }

    # Read specific versionScan info
    my ($names,$publishers,$platform) = '';
    my %check_methods;
    while (<WATCH>) {
      given ($_) {
        when (/^\s*\#\s*Names\s*:\s*(.*)$/) { $names = $1 };
        when (/^\s*\#\s*Publishers\s*:\s*(.*)$/) { $publishers = $1 };
        when (/^\s*\#\s*Platform\s*:\s*(.*)$/) { $platform = $1 };
        when (/^\s*\#\s*Check Methods\s*:\s*(.*)$/) { $check_methods{$platform} = $1 };
      }
    }

    if ($names eq '' or $publishers eq '' or !%check_methods ) {
      print "Cannot find versionScan info in watch file $soft_file, skipping.\n";
      next SOFT;
    }

    # Initialize uscan config
    my $uscanConfig = Devscripts::Uscan::Config->new->parse;

    # Follow all steps needed to get the software's latest version for every platform given
    my ($softwareId,$publisherId) = ('','');
    mkdir "tmp";
    chdir "tmp";
    foreach my $platform (keys %check_methods) {
        my $result = { 'status' => 0 };

        for my $step ( @{decode_json($check_methods{$platform})} ) {

            print "  Executing step $step->{'step'} for \"$soft\" on platform $platform.\n";
            given ( $step->{'step'} ) {
                when ('download')          { $result = $engine->download($result, $step->{'parameters'}) };
                when ('uscan')             { $result = $engine->uscan($result, $step->{'parameters'}, $uscanConfig, $soft) };
                when ('extract')           { $result = $engine->extract($result, $step->{'parameters'}) };
                when ('inspect')           { $result = $engine->inspect($result, $step->{'parameters'}) };
            }

            # Make sure step was OK
            if ( $result->{status} ) {
                print "Error while trying executing step $step->{'step'} for $soft\n";
                next SOFT;
            }

        }

        print "  Found version $result->{'last_version'}.\n";

        chdir "..";
	rmtree 'tmp';
    }

}
  
=head1 NAME

watchfiles-test.pl - Test watch files

=head1 SYNOPSIS

From the top of the versionScan project, type:

  perl -Ilib script/watchfiles-test.pl

=head1 DESCRIPTION

Validate a watch file for versionScan.

=head1 AUTHORS

Cyrille Bollu <cyrille@bollu.be>

=head1 COPYRIGHT

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
