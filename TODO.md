# Bugs

1. query interface doesn't work when vscan_sw_id contains a forward slash
2. solution for https://www.adobe.com/devnet-docs/acrobatetk/tools/Wizard/index.html
3. Add support for multi platforms in Platform watch file field (eg: "Windows 7, Windows 10/32,64bits")
4. UI: browse/ by publisher
5. UI: Add a search box in browse/ interface
6. Create proper test plan
7. UI: Change architecture in /vesion interface
8. Implement uptodate/ interface that just reply yes or no
9. Implement architecture table
10. Implement proper name aliases
11. Create a nice icon 
12. for ubuntu repositories, take into account the "-updates" repo
13. fix the https://packages.ubuntu.com/bionic/apache2 issue 
14. get rid of the vscan_sw table?
