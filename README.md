# versionScan

versionScan is a web service to find what's the latest version of a software.

versionScan is built on the Catalyst framework for its front-end and uses
Debian's uscan tool for its backend.

You may also have a look at [Anitya](https://release-monitoring.org) for a similar,
and much more comprehensive service.

# Installation

On Debian 9, do the following:

```
git clone https://framagit.org/StCyr/versionScan
apt-get install libipc-run-perl libcatalyst-perl libcatalyst-plugin-static-simple-perl \
  libcatalyst-plugin-configloader-perl \ libcatalyst-action-renderview-perl \
  libcatalyst-view-tt-perl libmoo-perl libfile-homedir-perl libjson-perl \ 
  libconfig-general-perl libversion-compare-perl libwin32-exe-perl \
  libxml-libxml-perl 
```

You may also want to create a `.devscripts` file in your home directory, with the 
following content, to avoid some warnings in your loags:

`USCAN_DOWNLOAD=no`

This will prevent the uscan libraries from trying to download archive of the
softwares being queried.

# Configuration

Copy the `versionscan.conf`file to `versionscan_local.conf`and adapt the later to match
 your environment. 

# Running versionScan

Since versionScan is built on the Catalyst framework, you can start it by simply 
issuing the following command:

`script/versionscan_server.pl`

## Running versionScan as an external FastCGI application

You can also run versionScan as an external FastCGI application.

In this case, you may want to have a look at the `contrib/versionScan_init-script` and 
`contrib/versionScan_apache-site.conf` files.

# Documentation

An instance of versionScan is running at https://version-originale.org. Please refer to
http://version-originale.org/doc for documentation about versionScan.
