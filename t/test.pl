#!/usr/bin/perl

use Win32::Exe;

use lib '../lib';
use versionScan::Model::software;

# ensure we are using the included Desvscripts sources
no lib '/usr/share/perl5/Devscripts';

# For uscan
use Devscripts::Uscan::Config;
use Devscripts::Uscan::WatchFile;
use Devscripts::Versort;

# Intialise uscan
my $uscanConfig = Devscripts::Uscan::Config->new->parse_conf_files();

# Force download
$uscanConfig->{'download'} = 1;
# Force download in tmp directory
$uscanConfig->{'destdir'} = './tmp';
# Do not make orig.tar.gz 
$uscanConfig->{'symlink'} = 'no';

# Get newest winpcap installer
my $softInfo = versionScan::Model::software->getLatestVersion("riverbed_technology:winpcap",$uscanConfig);

# print newest winpcap version from the .exe's ProductInfo field.
my $df = './tmp/' . $softInfo->{'downloadfile'};
my $exe = Win32::Exe->new($df);
my $info = $exe->get_version_info;
print $info->{'ProductVersion'};

