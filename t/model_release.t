use strict;
use warnings;
use Test::More tests => 2;

use versionScan;

my $app = versionScan->new();

my $id = $app->model('release')->getCount();
ok($id > 0, 'getCount returns a positive value');

my @all = $app->model('release')->getAll();
ok(scalar @all > 0, 'getAll returns a positive value');

done_testing();
