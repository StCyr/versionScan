use strict;
use warnings;
use Test::More tests => 1;

use versionScan;

my $app = versionScan->new();

my $count = $app->model('software')->getCount();
ok($count > 0, 'getCount() returns a positive value');

done_testing();
