use strict;
use warnings;
use Test::More tests => 10;

use Catalyst::Test 'versionScan';
use versionScan::Controller::Root;

content_like('/apiv2/version?name=Notepad%2B%2B&publisher=Notepad%2B%2B%20Team','/"last_version":"[\d\.]+"/', 'Returns a proper response when queried for software with special characters (Notepad++)' );

content_like('/apiv2/version?name=7-zip&publisher=7-zip','/"last_version":"[\d\.]+"/', 'Returns a proper response when queried for version of "name" = "7-zip" and "publisher" = "7-zip"' );

content_like('/apiv2/version?name=7-zip&publisher=Igor%20Pavlov','/"last_version":"[\d\.]+"/', 'Returns a proper response when queried for version of "name" = "7-zip" and "publisher" = "Igor Pavlov"' );

content_like('/apiv2/version?name=Inkscape&publisher=Inkscape%20Project','/"last_version":"[\d\.]+"/', 'Returns a proper response when queried for version of "name" = "Inkscape" and "publisher" = "Inkscape Project"' );

content_like('/apiv2/version?name=Chrome&publisher=Google&platform=Windows','/"last_version":"[\d\.]+"/', 'Returns a proper response when queried for version of "name" = "Chrome", "publisher" = "Google", and "platform" = "Windows"' );

content_like('/apiv2/version?name=chrome&publisher=google&platform=xyz','/"Error":"Unknown platform"/', 'Returns "unknown platform" error when platform is not known' );

content_like('/apiv2/version?name=7-zip','/"last_version":"[\d\.]+"/', 'Returns a proper response when queried for version of "name" = "7-zip"' );

content_like('/apiv2/version?name=dummy_foobar','/"Error/', 'Returns an error when apiv2/version is queried for an unknown software' );

content_like('/apiv2/version?publisher=7-zip','/"Error"/', 'Returns an error when apiv2/version is queried without the "name" argument' );

ok( request('/api/browse')->is_success, ' API browse requests work' );

done_testing();
