use strict;
use warnings;
use Test::More tests => 11;

use versionScan;

use Data::Dumper;

my $app = versionScan->new();

my $id = $app->model('software')->getLastID();
ok($id > 0, 'getLastID returns a positive value');

my $count = $app->model('software')->getCount();
ok($count > 0, 'getCount returns a positive value');

$id = $app->model('software')->getIdFromName('7-zip');
ok($id > 0, 'getIdFromName returns a positive value for name "7-zip"');

my @softwareList = $app->model('software')->getKnownSoftwareList(1);
ok(scalar @softwareList > 0, 'getKnownSoftwareList returns an array');

my $versionInfo = $app->model('software')->getLatestVersion($app, '7-zip:7-zip');
ok($versionInfo->{'last_version'} ne '', "getLatestVersion without a platform argument works (last_version = $versionInfo->{'last_version'})");

$versionInfo = $app->model('software')->getLatestVersion($app, 'google:chrome','Windows');
ok($versionInfo->{'last_version'} ne '', "getLatestVersion witht a platform argument works (last_version = $versionInfo->{'last_version'})");

my $swName = $app->model('software')->getNameFromVscanID('7-zip:7-zip');
ok($swName eq '7-zip', 'getNameFromVscanID() works');

my $vscanID = $app->model('software')->getVscanID($app, '7-zip', '7-zip');
ok($vscanID eq '7-zip:7-zip', "getVscanID() works (vscanID is $vscanID)");

my $error = $app->model('software')->getVscanID($app, 'foobar', '7-zip');
ok($error eq 'Error: unknown software foobar', "getVscanID() returns an error when the software is not known ($error)");

$error = $app->model('software')->getVscanID($app, '7-zip', 'foobar');
ok($error eq 'Error: unknown publisher foobar', "getVscanID() returns an error when the publisher is not known ($error)");

$error = $app->model('software')->getLatestVersion($app, 'google:chrome', 'foobar');
ok($error->{'Error'} eq 'Unknown platform', "getLatestVersion() returns an error when the platform is not known ($error->{'Error'})");

done_testing();
