use strict;
use warnings;
use Test::More;

use Catalyst::Test 'versionScan';
use versionScan::Controller::Root;

content_like('/api/query/7-zip:7-zip','/"last_version":"[\d\.]+"/', ' Returns a proper response for an API query request for 7-zip:7-zip' );

ok( request('/api/browse')->is_success, ' API browse requests work' );

done_testing();
