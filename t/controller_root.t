use strict;
use warnings;

use Test::More tests => 5;

use Catalyst::Test 'versionScan';

ok(request('/')->is_success, 'Index page works');
my $content = get('/');
ok( not ($content =~ m/badge-pill"><\/span>/gm), 'All stats on the index page look OK');

content_like('/version?name=7-zip&publisher=Igor%20Pavlov', '/The last version of 7-zip on platform \"\" is <b>[\d\.]+/', 'A query with parameters "name" equal to "7-zip" and "publisher" equal to "Igor Pavlov" returns the latest version' );
content_like('/version', '/Error/', 'A query with with no "name" parameter returns an error' );

content_like('/browse?page=7','/7-zip/', ' Browse request with page="7" should show 7-zip' );

done_testing();
