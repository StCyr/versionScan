use strict;
use warnings;
use Test::More tests => 1;

use versionScan;

use Data::Dumper;

my $app = versionScan->new();

my @list = $app->model('vscansw')->getAll();
ok(@list, 'getAll returns an array');

done_testing();
