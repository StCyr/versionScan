use strict;
use warnings;
use Test::More tests => 2;

use versionScan;

my $app = versionScan->new();

my @platformList = $app->model('platform')->getAll();
ok(scalar @platformList > 0, 'getAll() returns an array');

my $count = $app->model('platform')->getCount();
ok($count > 0, 'getCount returns a positive value');

done_testing();
