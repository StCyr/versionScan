package versionScan::Model::DBHelper;

use strict;
use warnings;

use parent 'Catalyst::Model::DBI';

=head1 NAME

versionScan::Model::DBHelper - Catalyst DBI Model Class

=head1 DESCRIPTION

Empty class only used to store the DB config 

=head1 AUTHOR

Cyrille Bollu <cyrille@bollu.be>

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
