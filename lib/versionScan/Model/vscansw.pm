package versionScan::Model::vscansw;

use strict;
use warnings;
use parent 'Catalyst::Model::DBI';

__PACKAGE__->config(
	dsn => versionScan->config->{'Model::DBHelper'}->{dsn},
	username => versionScan->config->{'Model::DBHelper'}->{username},	
	password => versionScan->config->{'Model::DBHelper'}->{password},
);

=head1 NAME

versionScan::Model::vscansw - Catalyst DBI Model Class

=head1 SYNOPSIS

See L<versionScan>

=head1 DESCRIPTION

Catalyst DBI Model Class.

=head1 AUTHOR

Cyrille Bollu <cyrille@bollu.be>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

=head2 getAll

Get all vscan software known

  my @vscanSotfwares = getAll();

=cut

sub getAll {
    my ( $self ) = @_;

    my $sth = $self->dbh->prepare('SELECT vscan_sw_id FROM vscan_sw');
    $sth->execute();

    my @vscanSoftwares;
    while (my @row = $sth->fetchrow_array) {
	push @vscanSoftwares, $row[0];
    }

    return @vscanSoftwares;

}


1;
