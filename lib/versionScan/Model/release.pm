package versionScan::Model::release;

use strict;
use warnings;

use parent 'Catalyst::Model::DBI';

__PACKAGE__->config(
        dsn => versionScan->config->{'Model::DBHelper'}->{dsn},
        username => versionScan->config->{'Model::DBHelper'}->{username},
        password => versionScan->config->{'Model::DBHelper'}->{password},
);

=head1 NAME

versionScan::Model::release - Catalyst DBI Model Class

=head1 SYNOPSIS

See L<versionScan>

=head1 DESCRIPTION

Catalyst DBI Model Class.

=head1 AUTHOR

Cyrille Bollu <cyrille@bollu.be>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

=head2 getCount

Get the number of known release

  my $count = getCount();

=cut

sub getCount {
  my ( $self ) = @_;

  my $sth = $self->dbh->prepare('SELECT COUNT(vscan_sw_id) FROM releases');
  $sth->execute();

  my @row = $sth->fetchrow_array;

  return $row[0];
}

=head2 getAll

Get All releases' details

  my @releases = getAll();

Returns nothing in case of error

=cut

sub getAll {
    my ( $self ) = @_;

    # Get all releases of vscan software vscanSwID
    my $sth = $self->dbh->prepare('SELECT vscan_sw_id, platform_id, architecture, check_method FROM releases ORDER BY last_check ASC');
    $sth->execute();

    # Create the @releases array
    my @releases;
    while (my @row = $sth->fetchrow_array) {
	my %release = (
	    vscan_sw_id   => $row[0],
	    platform_id   => $row[1],
	    architecture  => $row[2],
	    check_methods => $row[3],
	);
	push @releases, \%release;
    }

    return @releases;
}


=head2 getDetails

Get the release details of a vscan software
Returns an array with all matching releases

  my @releases = getDetails($vscanSwID);

Returns nothing in case of error

=cut

sub getDetails {
    my ( $self, $vscanSwID ) = @_;

    # vscanSwID is mandatory
    return unless defined $vscanSwID;

    # Get all releases of vscan software vscanSwID
    my $sth = $self->dbh->prepare('SELECT vscan_sw_id, platform_id, architecture, check_method FROM releases WHERE vscan_sw_id = ?');
    $sth->execute($vscanSwID);

    # Create the @releases array
    my @releases;
    while (my @row = $sth->fetchrow_array) {
	my %release = (
	    vscan_sw_id   => $row[0],
	    platform_id   => $row[1],
	    architecture  => $row[2],
	    check_methods => $row[3],
	);
	push @releases, \%release;
    }

    return @releases;
}


=head2 delete

Delete all releases of a vscan software

  delete($vscanSwID);

=cut

sub delete {
	my ( $self, $vscanSwID, $platformId ) = @_;

	# vscanSwID is mandatory
	return unless defined $vscanSwID;

	# Delete releases
	my $sth;

	if (defined $platformId) {
		$sth = $self->dbh->prepare('DELETE FROM releases WHERE vscan_sw_id = ? AND platform_id = ?');
		$sth->execute($vscanSwID, $platformId);
	} else {
		$sth = $self->dbh->prepare('DELETE FROM releases WHERE vscan_sw_id = ?');
		$sth->execute($vscanSwID);
	}

	return;
}

=head2 isUptodate

Returns 0 (false) when the queried release doesn't exist or its check_method has changed

    isUptodate($vscanSwID, $platformId, $architecture, $check_method)

=cut

sub isUptodate {
    my ($self, $vscanSwId, $platformId,$architecture, $check_method) = @_;

    my $sth = $self->dbh->prepare('SELECT * FROM releases WHERE vscan_sw_id = ? AND platform_id = ? and architecture = ? and check_method = ?');
    $sth->execute($vscanSwId, $platformId, $architecture, $check_method);
    if ($sth->rows == 0) {
        return 1;
    } else {
        return 0;
    }
}

=head2 updateInfo

Update a release's information

    updateInfo($vscanSwID, $platformId, $architecture, $version, $check_method, $releaseUrl)

=cut

sub updateInfo {
  my ($self, $vscan_sw_id, $platformId, $architecture, $version, $check_method, $releaseUrl) = @_;

  # Does the release already exist?
  my $sth = $self->dbh->prepare('SELECT count(vscan_sw_id) FROM releases WHERE vscan_sw_id = ? AND platform_id = ? AND architecture = ?');
  $sth->execute($vscan_sw_id, $platformId, $architecture);
  my @row = $sth->fetchrow_array;
  if ($row[0] == 0) {

    # Insert new release
    $sth = $self->dbh->prepare('INSERT INTO releases SET vscan_sw_id = ?, platform_id = ?, architecture = ?, last_version = ?, last_check = NOW(), last_check_successful = NOW(), check_method = ?, url = ?');
    $sth->execute($vscan_sw_id, $platformId, $architecture, $version, $check_method, $releaseUrl);
  } else {

    # Existing release
    if ($version ne '') {
      # Check was successful
      $sth = $self->dbh->prepare('UPDATE releases SET last_version = ?, last_check = NOW(), last_check_successful = NOW(), check_method = ?, url = ? WHERE vscan_sw_id = ? AND platform_id = ? AND architecture = ?');
      $sth->execute($version, $check_method, $releaseUrl, $vscan_sw_id, $platformId, $architecture);
    } else {
      # Check was unsuccessful
      $sth = $self->dbh->prepare('UPDATE releases SET last_check = NOW(), check_method = ? WHERE vscan_sw_id = ? AND platform_id = ? AND architecture = ?');
      $sth->execute($vscan_sw_id, $check_method, $platformId, $architecture);
    }

  }

  return;
}

1;
