package versionScan::Model::publisher;

use strict;
use warnings;

use parent 'Catalyst::Model::DBI';

__PACKAGE__->config(
        dsn => versionScan->config->{'Model::DBHelper'}->{dsn},
        username => versionScan->config->{'Model::DBHelper'}->{username},
        password => versionScan->config->{'Model::DBHelper'}->{password},
);

=head1 NAME

versionScan::Model::publisher - Catalyst Model

=head1 SYNOPSIS

See L<versionScan>

=head1 DESCRIPTION

Catalyst Model Class.

=head1 AUTHOR

Cyrille Bollu <cyrille@bollu.be>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

=head2 getCount

Get the number of distinct publishers known

  my $nb_pub = getCount();

=cut

sub getCount {
    my ( $self ) = @_;

    my $sth = $self->dbh->prepare('SELECT COUNT(DISTINCT pub_id) FROM publisher');
    $sth->execute();
    my @row = $sth->fetchrow_array;

    return $row[0];

}

=head2 createID

Create a new publisher ID

  my $pubID = createID($name);

=cut

sub createID {
    my ( $self, $name ) = @_;

    my $pubID = $self->getLastID();
    $pubID += 1;
    $self->dbh->do('INSERT INTO publisher SET pub_id = ?, pub_name = ?', undef, $pubID, $name );

    return $pubID;

}

=head2 getLastID

  my $lastID = getLastID();

=cut

sub getLastID {
  my ( $self ) = @_;

  my $sth = $self->dbh->prepare('SELECT MAX(pub_id) FROM publisher');
  $sth->execute();
  my @lastId = $sth->fetchrow_array();
  $lastId[0] = 0 unless @lastId;
  return $lastId[0];
}

=head2 getNamesFromID

Get all known publisher names for an ID

  my @pubList = getNamesFromID($ID);

Returns nothing when the ID isn't found

=cut

sub getNamesFromID {
    my ( $self, $ID ) = @_;

    my $sth = $self->dbh->prepare('SELECT pub_name FROM publisher where pub_id = ?');
    $sth->execute($ID);

    my $pubList;
    while ( my @row = $sth->fetchrow_array() ) {
         $pubList->{$row[0]} = $row[0];
    }

    return $pubList;

}

=head2 getIdFromName

Get a publisher's ID based on its name

  my $pubID = getIdFromName($name);

Returns nothing when the publisher name isn't found

=cut

sub getIdFromName {
    my ( $self, $name ) = @_;

    my $sth = $self->dbh->prepare('SELECT pub_id FROM publisher where pub_name = ?');
    $sth->execute($name);

    my @pubID = $sth->fetchrow_array();

    if (@pubID) {
        return $pubID[0];
    } else {
        return;
    }

}

=head2 addAlias

Add a new alias to a publisher

  addAlias($id, $name);

=cut

sub addAlias {
  my ($self, $id, $name) = @_;

  $self->dbh->do('INSERT INTO publisher SET pub_id = ?, pub_name = ?', undef, $id, $name);

  return;
}

1;
