package versionScan::Model::platform;

use strict;
use warnings;

use parent 'Catalyst::Model::DBI';

__PACKAGE__->config(
	dsn => versionScan->config->{'Model::DBHelper'}->{dsn},
	username => versionScan->config->{'Model::DBHelper'}->{username},	
	password => versionScan->config->{'Model::DBHelper'}->{password},
);

=head1 NAME

versionScan::Model::platform - Catalyst Model

=head1 SYNOPSIS

See L<versionScan>

=head1 DESCRIPTION

DBI Model Class.

=head1 AUTHOR

Cyrille Bollu <cyrille@bolu.be>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

=head2 getAll

Get a list of all platforms known

  my @platformList = getAll();

=cut

sub getAll {
    my ( $self ) = @_;

    my @platformList = ();
    my $sth = $self->dbh->prepare('SELECT platform_name FROM platform');
    $sth->execute();
    while ( my @row = $sth->fetchrow_array ) {
        push @platformList, $row[0];
    }

    return \@platformList;

}

=head2 getCount

Get the number of distinct platforms known

  my $nb_platform = getCount();

=cut

sub getCount {
    my ( $self ) = @_;

    my $sth = $self->dbh->prepare('SELECT COUNT(DISTINCT platform_id) FROM platform');
    $sth->execute();
    my @row = $sth->fetchrow_array;

    return $row[0];

}

=head2 addAlias

Add a name alias $name to platform with ID $id (if needed)

  addAlias($id, $name);

=cut

sub addAlias {
  my ($self, $id, $name) = @_;

  # Insert platform if not already in DB.
  my $sth = $self->dbh->prepare('SELECT platform_id FROM platform WHERE platform_id= ? AND platform_name = ?');
  $sth->execute($id,$name);
  if ($sth->rows == 0) {
    $self->dbh->do('INSERT INTO platform SET platform_id = ?, platform_name = ?', undef, $id, $name);
  }

  return;
}

=head2 createID

Create a new platform ID

  my $pID = createID($platform_name);

=cut

sub createID {
    my ( $self, $name ) = @_;

    my $pID = $self->getLastID();
    $pID += 1;
    $self->dbh->do('INSERT INTO platform SET platform_id = ?, platform_name = ?', undef, $pID, $name );

    return $pID;

}

=head2 getIdFromName

Get a platofrm's ID based on its name.

  my $pID = getIdFromName($name);

Returns nothing when the platfrom isn't found
=cut

sub getIdFromName {
    my ( $self, $name ) = @_;

    my $sth = $self->dbh->prepare('SELECT platform_id FROM platform where platform_name = ?');
    $sth->execute($name);

    my @pID = $sth->fetchrow_array();

    if (@pID) {
        return $pID[0];
    } else {
        return;
    }

}

=head2 getLastID

Get the last known platform ID

  my $lastID = getLastID();

=cut

sub getLastID {
  my ( $self ) = @_;

  my $sth = $self->dbh->prepare('SELECT MAX(platform_id) FROM platform');
  $sth->execute();
  my @lastId = $sth->fetchrow_array();
  $lastId[0] = 0 unless @lastId;
  return $lastId[0];
}

1;
