package versionScan::Model::software;

use strict;
use warnings;

use parent 'Catalyst::Model::DBI';

use Version::Compare qw(version_compare);

__PACKAGE__->config(
	dsn => versionScan->config->{'Model::DBHelper'}->{dsn},
	username => versionScan->config->{'Model::DBHelper'}->{username},	
	password => versionScan->config->{'Model::DBHelper'}->{password},
);

=head1 NAME

versionScan::Model::software - Catalyst DBI Model Class

=head1 SYNOPSIS

See L<versionScan>

=head1 DESCRIPTION

Catalyst DBI Model Class.

=head1 AUTHOR

Cyrille Bollu <cyrille@bollu.be>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

=head2 getLatestVersion

Get the latest version of a software.

    my %latestVersionInfo = getLatestVersion($c, $vscanSwID, $platform)

=cut

sub getLatestVersion {
    my ( $self, $c, $vscanSwID, $platform ) = @_;

    my $version = '';
    my $lastVersion = '';
    my $lastVersionOK = '';
    my $url = '';
    my $sth;

    if ( not defined $platform or $platform eq '' or $platform eq 'any') {
        $sth = $self->dbh->prepare('SELECT last_version, last_check, last_check_successful, url FROM releases WHERE vscan_sw_id = ?');
        $sth->execute($vscanSwID);
    } else {
	my $pID = $c->model('platform')->getIdFromName($platform);
	if (not defined $pID) {
	    return { 'Error' => "Unknown platform" };
        }    
        $sth = $self->dbh->prepare('SELECT last_version, last_check, last_check_successful, url FROM releases WHERE vscan_sw_id = ? AND platform_id = ?');
        $sth->execute($vscanSwID, $pID);
    }

    # When there are several releases found take the highest known version by default
    while ( my @row = $sth->fetchrow_array ) {
	if ( Version::Compare::version_compare($row[0],$version) == 1 ) {
            $version = $row[0];
            $lastVersion = $row[1];
	    $lastVersionOK = $row[2];
	    $url = $row[3];
	}
    }

   return { 'software'     => $vscanSwID,
            'last_version' => $version,
            'last_check'   => $lastVersion,
            'last_success' => $lastVersionOK,
	    'url'	   => $url,
          };
    
}

=head2 getInitialList

Get the list of the initials of all software known for a specific
platform (if specified).

  my @softwareList = getInitialList($platformID);

=cut

sub getInitialList {
    my ( $self, $platformID ) = @_;

    # Execute query depending on the $platformID parameter
    my $sth;
    if ( not defined $platformID or $platformID eq '') {
        $sth = $self->dbh->prepare('SELECT DISTINCT LEFT(sw_name,1) FROM sw ORDER BY 1 ASC');
        $sth->execute();
    } else {
        $sth = $self->dbh->prepare('SELECT DISTINCT LEFT(sw_name,1) FROM vscan_sw LEFT JOIN publisher ON vscan_sw.pub_id = publisher.pub_id INNER JOIN sw ON vscan_sw.sw_id = sw.sw_id INNER JOIN releases ON vscan_sw.vscan_sw_id = releases.vscan_sw_id WHERE releases.platform_id = ? ORDER BY 1 ASC');
        $sth->execute($platformID);
    }

    # Create the list of known softwares from the query's result
    my @initials = ();
    while ( my @row = $sth->fetchrow_array ) {
        push @initials, $row[0];
    }

    return @initials;

}

=head2 getKnownSoftwareList

Get the list of software known by this instance of versionScan for 
a specific platform (optional) and starting by a specific initial (optional)

  my @softwareList = getKnownSoftwareList($platformID, $initial);

=cut

sub getKnownSoftwareList {
    my ( $self, $pID, $initial ) = @_;

    my $sth;
    my $sql = 'SELECT sw_name, pub_name FROM vscan_sw LEFT JOIN publisher ON vscan_sw.pub_id = publisher.pub_id INNER JOIN sw ON vscan_sw.sw_id = sw.sw_id';

    # Execute query depending on whether a platform or an initial has been given
    if ( not defined $pID or $pID eq '') {
	if ( not defined $initial or $initial eq '' ) {
            $sth = $self->dbh->prepare($sql);
            $sth->execute();
	} else {
	    $initial .= '%';
	    $sql .= ' WHERE sw_name LIKE ?';
            $sth = $self->dbh->prepare($sql);
            $sth->execute($initial);
	}
    } else {
        $sql .= ' INNER JOIN releases ON vscan_sw.vscan_sw_id = releases.vscan_sw_id WHERE releases.platform_id = ?';
	if ( not defined $initial or $initial eq '' ) {
            $sth = $self->dbh->prepare($sql);
            $sth->execute($pID);
	} else {
	    $initial .= '%';
	    $sql .= ' AND sw_name LIKE ?';
            $sth = $self->dbh->prepare($sql);
            $sth->execute($pID, $initial);
	}
    }

    # Create the list of known softwares from the query's result
    my @softwareList = ();
    while ( my @row = $sth->fetchrow_array ) {
      push @softwareList, {
        Name      => $row[0],
        Publisher => $row[1],
      };
    }

    return @softwareList;

}

=head2 getSwInErrorCount

Get the number of softwares that couldn't be successfuly checked for the last 7 days or whose last version isn't known

  my $nb_sw = getSwInErrorCount();

=cut

sub getSwInErrorCount {
    my ( $self ) = @_;

    my $sth = $self->dbh->prepare('SELECT COUNT(vscan_sw_id) FROM releases WHERE DATEDIFF(NOW(),last_check_successful) >= 7 OR last_version IS NULL');	
    $sth->execute();
    my @row = $sth->fetchrow_array;

    return $row[0];

}

=head2 getCount

Get the number of distinct softwares known

  my $nb_sw = getCount();

=cut

sub getCount {
    my ( $self ) = @_;

    my $sth = $self->dbh->prepare('SELECT COUNT(DISTINCT sw_id) FROM sw');
    $sth->execute();
    my @row = $sth->fetchrow_array;

    return $row[0];

}

=head2 addAlias

Insert a name alias $name to software with ID $id (if not already there)

  addAlias($id, $name);

=cut

sub addAlias {
  my ($self, $id, $name) = @_;

  # Insert software if not already in DB.
  my $sth = $self->dbh->prepare('SELECT sw_id FROM sw WHERE sw_id= ? AND sw_name = ?');
  $sth->execute($id,$name);
  if ($sth->rows == 0) {
    $self->dbh->do('INSERT INTO sw SET sw_id = ?, sw_name = ?', undef, $id, $name);
  }

  return;
}

=head2 createID

Create a new software ID

  my $softID = createID($name);

=cut

sub createID {
    my ( $self, $name ) = @_;

    my $softID = $self->getLastID();
    $softID += 1;
    $self->dbh->do('INSERT INTO sw SET sw_id = ?, sw_name = ?', undef, $softID, $name );

    return $softID;

}

=head2 getIdFromName

Get the softwareID from a software name.

  my $softID = getIdFromName($name);

Returns nothing when the software name isn't found
=cut

sub getIdFromName {
    my ( $self, $name ) = @_;

    my $sth = $self->dbh->prepare('SELECT sw_id FROM sw where sw_name = ?');
    $sth->execute($name);

    my @softID = $sth->fetchrow_array();

    if (@softID) {
        return $softID[0];
    } else {
        return;
    }

}

=head2 getLastID

Get the last known software ID

  my $lastID = getLastID();

=cut

sub getLastID {
  my ( $self ) = @_;

  my $sth = $self->dbh->prepare('SELECT MAX(sw_id) FROM sw');
  $sth->execute();

  my @lastId = $sth->fetchrow_array();
  $lastId[0] = 0 unless @lastId;

  return $lastId[0];
}

=head2 getNameFromVscanID

Get the name of a software

  my $swName = getNameFromVscanID($vscan_sw_id);

=cut

sub getNameFromVscanID {
    my ( $self, $vscanSwID ) = @_;

    my $sth;
    $sth = $self->dbh->prepare('SELECT sw_name FROM vscan_sw LEFT JOIN sw ON vscan_sw.sw_id = sw.sw_id WHERE vscan_sw_id = ?');
    $sth->execute($vscanSwID);

    # TODO: Handle case where there are several results
    my @row = $sth->fetchrow_array;
    my $swName = $row[0];

    return $swName;

}

=head2 createVscanID

Create a versionScan ID from a software/publisher tulpe

  my $vscanID = createVscanID($c, $sw, $pub)

=cut

sub createVscanID {
    my ( $self, $c, $sw, $pub ) = @_;

    # Get a software ID
    my $swID = $self->getIdFromName($sw);
    if (not defined $swID) {
	$swID = $self->createID($sw); 
    }

    # Get a publisher ID
    my $pubID = $c->model('publisher')->getIdFromName($pub);
    if (not defined $pubID) {
	$pubID = $c->model('publisher')->createID($pub); 
    }

    # Compute a vscan ID
    my $vscanID = lc("$pub" . ":" . "$sw");
    $vscanID =~ tr/ /_/;

    # Create the DB entry
    my $sth = $self->dbh->prepare('INSERT INTO vscan_sw SET vscan_sw_id = ?, sw_id = ?, pub_id = ?');
    $sth->execute($vscanID, $swID, $pubID);

    return $vscanID;

}

=head2 getVscanID

Get the versionScan ID of a software.

  my $vscanID = getVscanID($c, $sw, $pub)

In case of error, returns a string starting by "Error"

=cut

sub getVscanID {
    my ( $self, $c, $name, $pub ) = @_;

    my $soft_id = $self->getIdFromName($name);
    if ( not defined $soft_id ) {
	return "Error: unknown software $name";
    }

    # Look for versionScan ID depending on whether publisher name was given or not 
    my $sth;
    if (not defined $pub or $pub eq '') {
        $sth = $self->dbh->prepare('SELECT vscan_sw_id FROM vscan_sw WHERE sw_id = ?');
        $sth->execute($soft_id);
    } else {
        my $pub_id = $c->model('publisher')->getIdFromName($pub);
        if ( not defined $pub_id ) {
	    return "Error: unknown publisher $pub";
        }
        $sth = $self->dbh->prepare('SELECT vscan_sw_id FROM vscan_sw WHERE sw_id = ? AND pub_id = ?');
        $sth->execute($soft_id,$pub_id);
    }

    if ($sth->rows > 1) {
        return "Error: request matched several vscan softwares";
    }

    my @row = $sth->fetchrow_array;
    my $vscan_id = $row[0];
    if ( not defined $vscan_id ) {
        return "Error: request matched no vscan software";
    }

    return $vscan_id;

}

1;
