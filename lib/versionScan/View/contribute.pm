package versionScan::View::contribute;
use Moose;
use namespace::autoclean;

extends 'Catalyst::View::TT';

__PACKAGE__->config(
    TEMPLATE_EXTENSION => '.tt',
    render_die => 1,
);

=head1 NAME

versionScan::View::contribute - TT View for versionScan

=head1 DESCRIPTION

TT View for versionScan.

=head1 SEE ALSO

L<versionScan>

=head1 AUTHOR

root

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
