package versionScan::Controller::Root;
use Moose;
use namespace::autoclean;

use feature "switch";
no warnings 'experimental::smartmatch';

BEGIN { extends 'Catalyst::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=encoding utf-8

=head1 NAME

versionScan::Controller::Root - Root Controller for versionScan

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    # Set the template to use for rendering the page
    $c->stash->{template} = 'templates/index.tt';

    # view to be used 
    $c->stash->{current_view} = 'index';

    # Set up data for rendering of TT template
    $c->stash->{urlBrowse} = $c->config->{siteURL} . '/browse';
    $c->stash->{urlAPIDoc} = $c->config->{siteURL} . '/doc#API';
    $c->stash->{siteName} = $c->config->{siteName};
    $c->stash->{nbRelease} = $c->model('release')->getCount();
    $c->stash->{nbSoft} = $c->model('software')->getCount();
    $c->stash->{nbPub} = $c->model('publisher')->getCount();
    $c->stash->{nbPlatform} = $c->model('platform')->getCount();

}

=head2 admin

The admin page (/admin)

=cut

sub admin :Local :Args(0) {
    my ( $self, $c ) = @_;

    # Set the template to use for rendering the page
    $c->stash->{template} = 'templates/admin.tt';

    # view to be used 
    $c->stash->{current_view} = 'admin';

    # Set up data for rendering of TT template
    $c->stash->{siteName} = $c->config->{siteName};
    $c->stash->{nbSoft}  = $c->model('software')->getCount();
    $c->stash->{nbError} = $c->model('software')->getSwInErrorCount();

}

=head2 version

Extended interface to query the last version of a software

=cut

sub version :Local {
    my ( $self, $c ) = @_;

    # Set the page and template to use for rendering the page
    $c->stash->{template} = 'templates/query.tt';
    $c->stash->{current_view} = 'query';

    # This will be the page's title
    $c->stash->{title} = 'Latest version';

    # Get mandatory parameter
    if ( not defined $c->req->param('name') ) {
       	$c->stash->{error} = 'No software name provided';
	return;
    }
    my $soft = $c->request->param('name');

    # Get optional parameters
    my $pub      = $c->req->param('publisher') || ''; 
    my $platform = $c->req->param('platform')  || ''; 
    my $arch     = $c->req->param('arch')      || ''; 

    # Get the software queried from the name and publisher parameters
    my $vscan_id = $c->model('software')->getVscanID( $c, $soft, $pub );

    # Get latest version info
    $c->stash->{data} = $c->model('software')->getLatestVersion($c, $vscan_id, $platform);

    # Get the software name
    $c->stash->{sw_name} = $soft;
    $c->stash->{platform} = $platform;

}

=head2 browse

Interface to browse the list of known softwares
(those for which latest version can be queried)

Creates a list of all softwares starting by the value of the 
request's page parameter, optionaly filtered by the value of
the request's platform parameter. 

=cut

sub browse :Local {
    my ( $self, $c ) = @_;

    my @filteredSoftwareList;
    my $platformID;

    # Get request parameters
    my $page = $c->req->param('page') || '';
    my $platform = $c->req->param('platform') || 'Windows';

    # Set the template and view to be used for rendering the page
    $c->stash->{template} = 'templates/browse.tt';
    $c->stash->{current_view} = 'browse';

    # We need the URL of the local API interface for links 
    $c->stash->{siteName} = $c->config->{siteName};
    $c->stash->{urlApiVersion} = $c->config->{siteURL} . '/version?';

    # Get platform's ID 
    if ( $platform eq 'any' ) {
        $platformID = ''
    } else {
	# TODO: Handle case wgeb null
        $platformID = $c->model('platform')->getIdFromName($platform);
    }

    # Get the initials of the softwares to create a pagination bar
    my @initials = $c->model('software')->getInitialList($platformID);

    # Which page should be shown?
    my $currentPage = $page;
    if ( $page eq '' or not $page ~~ @initials ) {
        $currentPage = $initials[0];
    }

    # Get the list of known softwares by this versionScan instance
    my @softwareList = $c->model('software')->getKnownSoftwareList($platformID, $currentPage);

    # Get the list of platform known
    $c->stash->{platformList} = $c->model('platform')->getAll();

    # To know the currently viewed page
    $c->stash->{currentPage} = $currentPage;
    $c->stash->{currentPlatform} = $platform;

    # The actual data
    $c->stash->{initials} = \@initials;
    $c->stash->{softwares} = \@softwareList;

}

=head2 doc

Documentation interface.

=cut

sub doc :Local :Args(0) {
    my ( $self, $c ) = @_;

    # Set the template and view to be used for rendering the page
    $c->stash->{template} = 'templates/doc.tt';
    $c->stash->{current_view} = 'doc';

    $c->stash->{siteName} = $c->config->{siteName};

}

=head2 contribute

Interface to explain visitors how to help this project.

=cut

sub contribute :Local :Args(0) {
    my ( $self, $c ) = @_;

    # Set the template to use for rendering the page
    $c->stash->{template} = 'templates/contribute.tt';

    # view to be used 
    $c->stash->{current_view} = 'contribute';

    # Set up data for rendering of TT template
    $c->stash->{urlBrowse} =$c->config->{siteURL} . '/browse/';
    $c->stash->{siteName} = $c->config->{siteName};
    $c->stash->{siteContact} = $c->config->{siteContact};

}

=head2 default

Standard 404 error page

=cut

sub default :Path {
    my ( $self, $c ) = @_;
    $c->response->body( 'Page not found' );
    $c->response->status(404);
}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : ActionClass('RenderView') {}

=head1 AUTHOR

support,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
