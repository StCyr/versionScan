package versionScan::Controller::Api;
use JSON;
use Moose;
use namespace::autoclean;

# For uscan
use Devscripts::Uscan::Config;
use Devscripts::Versort;
use Devscripts::Uscan::WatchFile;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

versionScan::Controller::Api - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=head2 query

Interface to query the latest version of a software

=cut

sub query :Local :Args(1) {
    my ( $self, $c ) = @_;

    # Get the software queried
    my $soft = $c->request->arguments->[0];

    # Get the latest version information
    my $data = $c->model('software')->getLatestVersion($c, $soft);

    # Add a CORS header if specified in config
    if ($c->config->{cors}) {
        $c->response->header('Access-Control-Allow-Origin' => $c->config->{cors});
    };

    # Set it as response's body for the case where the path '/api/query' is called directly
    $c->response->body( encode_json($data) );
}

=head2 browse

Interface that returns the list of known softwares (those for which
latest version can be queried) in JSON format.

=cut

sub browse :Local :Args(0) {
    my ( $self, $c ) = @_;

    # Get the list of known softwares
    my @softwareList = $c->model('software')->getKnownSoftwareList();

    # Set it as response's body for the case where the path '/api/browse' is called directly
    $c->response->body( encode_json(\@softwareList) );
}

=encoding utf8

=head1 AUTHOR

support,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
