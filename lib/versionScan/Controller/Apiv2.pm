package versionScan::Controller::Apiv2;
use JSON;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

versionScan::Controller::Apiv2 - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=head2 version

Interface to query the latest version of a software

Accepted parameters:

  - name (MANDATROY): The name of the software for which 
    the latest version number is requested.
  - publisher (OPTIONAL): The name of the software's publisher.
  - platform (OPTIONAL): The name of the platform considered (eg
    Windows, Mac OS X, Debian 9)
  - arch (OPTIONAL): The name of the architecture considered
    (eg: amd64, x86,arm)

=cut

sub version :Local :Args(0) {
    my ( $self, $c ) = @_;

    # Add a CORS header if specified in config
    if ($c->config->{cors}) {
        $c->response->header('Access-Control-Allow-Origin' => $c->config->{cors});
    };

    # Verify mandatory parameter
    if ( not defined $c->req->param('name') ) {
	my $data = {
	  'Error' => 'No software name provided',
	};
        $c->response->body( encode_json($data) );
	return;
    }

    # Get the name of software queried
    my $soft = $c->request->param('name');

    # Get optional parameters
    my $pub      = $c->req->param('publisher') || ''; 
    my $platform = $c->req->param('platform')  || ''; 
    my $arch     = $c->req->param('arch')      || ''; 

    # Get the latest version information
    my $vscan_id = $c->model('software')->getVscanID($c, $soft, $pub);
    if ( $vscan_id =~ /^Error/ ) {
	my $data = {
	  'Error' =>"Could not find software ($vscan_id)",
	};
        $c->response->body( encode_json($data) );
	return;
    }

    # Get the latest version information
    my $data = $c->model('software')->getLatestVersion($c, $vscan_id, $platform);

    # Set it as response's body for the case where the path '/apiv2/version' is called directly
    $c->response->body( encode_json($data) );
}

=head2 browse

Interface that returns the list of known softwares (those for which
latest version can be queried) in JSON format.

=cut

sub browse :Local :Args(0) {
    my ( $self, $c ) = @_;

    # Get the list of known softwares
    my @softwareList = $c->model('software')->getKnownSoftwareList();

    # Set it as response's body for the case where the path '/api/browse' is called directly
    $c->response->body( encode_json(\@softwareList) );
}

=encoding utf8

=head1 AUTHOR

support,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
