package versionScan::Core::Engine;

use strict;
use warnings;
use feature "switch";
no warnings 'experimental::smartmatch';

sub new{
    my ($class) = @_;
    my $self = bless {}, $class;
}

####################################################################
#
# check_methods functions
#
# ##################################################################

sub inspect {

  my ($self, $result, $params) = @_;

  my $method = $params->{'method'};
  my $file = $params->{'file'} || $result->{'file'};

  # TODO: Add error handling
  my $output;
  given ($method) {
    when (/msiinfo/) {
      ($result->{'last_version'}) = `$method suminfo $file` =~ /$params->{regexp}/
     };
    when (/win32::exe/) {
      # Exe files can be quite big
      if ( $params->{maxfilesize} and -s $file > $params->{maxfilesize} ) {
        truncate($file, $params->{maxfilesize});
      };
      my $info  = Win32::Exe->new($file)->get_version_info;
      $result->{'last_version'} = $info->{ $params->{field} };
    };
    when (/plist/) {
      # Get the next 'string' element after the 'key' element specified in parameter
      my $dom = XML::LibXML->load_xml( location => $file );
      $result->{'last_version'} = $dom->findnodes( "/plist/dict/key[text()='" . $params->{key} . "']/following-sibling::string[1]" );
    };

  }

  return $result;

}

# Extract a downloaded file
sub extract {

  my ($self, $result, $params) = @_;

  my $method = $params->{'method'};
  my $file = $params->{'file'} || $result->{'file'};
  my $outFile = $params->{'outfile'} || '';

  given ($method) {
    when (/cabextract/) { system("$method -q $file") };
    when (/unzip/)      { system("$method -o -q $file") };
    when (/dmg2img/)    { if ($outFile ne '') { system("$method -s $file -o $outFile") } else { system("$method -s $file") } };
    when (/7z/)         { system("$method x $file >/dev/null") };
    default             { $result->{status} = 1 };
  }

  return $result;
}

# direct download of a file using LWP
sub download {

  my ($self, $result, $params) = @_;

  my $ua = LWP::UserAgent->new;
  $ua->agent("VersionScan");

  my $headers = HTTP::Headers->new;
  $headers->header('Accept'  => '*/*');
  $headers->header('Referer' => $params->{'user_url'});

  my $req = HTTP::Request->new('GET', $params->{'url'}, $headers);

  my $res =  $ua->request($req, $params->{'file'});

  if (!$res->is_success) {
    $result->{'status'} = 1;
  }

  $result->{'file'} = $params->{'file'};
  $result->{'url'} = $params->{'user_url'};

  return $result;

}

# Use uscan methods to find out software's latest version
sub uscan {

  my ($self, $result, $params, $uscanConfig,$soft) = @_;

  # Create a temporary watch file
  unless (open WATCH,">./uscan") {
    print "Error while trying to create temporary watch file\n";
    $result->{'status'} = 1;
    return $result;
  }
  print WATCH "version=4\n";
  print WATCH "$params->{'watchline'}\n";
  close WATCH;

  # Process watch file using uscan
  my $wf = Devscripts::Uscan::WatchFile->new({
    config      => $uscanConfig,
    package     => $soft,
    pkg_dir     => ".",
    pkg_version => 0,
    watchfile   => "./uscan",
  });

  # Get latest version (copied from Devscripts::Uscan::WatchFile)
  my $wl = {};
  foreach $wl (@{ $wf->watchlines }) {

    # search latest version
    $wl->parse;
    $wl->search;

    $result->{'last_version'} = $wl->{'search_result'}->{'newversion'};
    $result->{'url'} = $wl->{'parse_result'}->{'base'};
    $result->{'status'} = $wl->{'status'};

  }

  return $result;

}

1;
