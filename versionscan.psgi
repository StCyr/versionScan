use strict;
use warnings;

use versionScan;

my $app = versionScan->apply_default_middlewares(versionScan->psgi_app);
$app;

